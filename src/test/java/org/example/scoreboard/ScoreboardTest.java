package org.example.scoreboard;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScoreboardTest {
    @org.junit.jupiter.api.Test
    void startMatch_shouldThrowAnException_whenTheMatchAlreadyExists() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch(new Team("Real Madrid"), new Team("Barcelona"));

        assertThrows(IllegalArgumentException.class, () -> scoreboard.startMatch(new Team("Real Madrid"), new Team("Barcelona")));
    }

    @org.junit.jupiter.api.Test
    void startMatch_shouldSetTheInitialScoreOfTheMatchToZero() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch(new Team("Real Madrid"), new Team("Barcelona"));

        List<Match> summary = scoreboard.getSummary();
        Match match = summary.get(0);
        assertEquals(0, match.getTotalScore());
    }

    @org.junit.jupiter.api.Test
    void updateScore_shouldThrowAnException_whenTheMatchDoesNotExist() {
        Scoreboard scoreboard = new Scoreboard();

        assertThrows(IllegalArgumentException.class, () -> scoreboard.updateScore(new Team("Real Madrid"), new Team("Barcelona"), 1, 0));
    }

    @org.junit.jupiter.api.Test
    void updateScore_shouldThrowAnException_whenTheMatchIsFinished() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam = new Team("Real Madrid");
        Team awayTeam = new Team("Barcelona");
        scoreboard.startMatch(homeTeam, awayTeam);
        scoreboard.finishMatch(homeTeam, awayTeam);

        assertThrows(IllegalArgumentException.class, () -> scoreboard.updateScore(homeTeam, awayTeam, 1, 0));
    }

    @org.junit.jupiter.api.Test
    void updateScore_shouldUpdateTheScoreOfTheMatch() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam = new Team("Real Madrid");
        Team awayTeam = new Team("Barcelona");
        scoreboard.startMatch(homeTeam, awayTeam);

        scoreboard.updateScore(homeTeam, awayTeam, 2, 1);

        List<Match> summary = scoreboard.getSummary();
        Match match = summary.get(0);
        assertEquals(2, match.homeTeamScore());
        assertEquals(1, match.awayTeamScore());
    }

    @org.junit.jupiter.api.Test
    void updateScore_shouldNotUpdateTheScoresOfOtherMatches() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeamOfMatchToUpdate = new Team("Real Madrid");
        Team awayTeamOfMatchToUpdate = new Team("Barcelona");
        scoreboard.startMatch(homeTeamOfMatchToUpdate, awayTeamOfMatchToUpdate);
        scoreboard.startMatch(new Team("Juventus"), new Team("Milan"));

        scoreboard.updateScore(homeTeamOfMatchToUpdate, awayTeamOfMatchToUpdate, 2, 1);

        List<Match> summary = scoreboard.getSummary();
        Match match = summary.get(1);
        assertEquals(0, match.getTotalScore());
    }

    @org.junit.jupiter.api.Test
    void updateScore_shouldNotUpdateMatchesAlreadyReturnedFromGetSummary() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam = new Team("Real Madrid");
        Team awayTeam = new Team("Barcelona");
        scoreboard.startMatch(homeTeam, awayTeam);
        Match matchReturnedFromGetSummary = scoreboard.getSummary().get(0);

        scoreboard.updateScore(homeTeam, awayTeam, 2, 1);

        assertEquals(0, matchReturnedFromGetSummary.homeTeamScore());
        assertEquals(0, matchReturnedFromGetSummary.awayTeamScore());
    }

    @org.junit.jupiter.api.Test
    void finishMatch_shouldThrowAnException_whenTheMatchDoesNotExist() {
        Scoreboard scoreboard = new Scoreboard();

        assertThrows(IllegalArgumentException.class, () -> scoreboard.finishMatch(new Team("Real Madrid"), new Team("Barcelona")));
    }

    @org.junit.jupiter.api.Test
    void finishMatch_shouldRemoveTheMatchFromTheSummary() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeamOfMatchToFinish = new Team("Real Madrid");
        Team awayTeamOfMatchToFinish = new Team("Barcelona");
        scoreboard.startMatch(homeTeamOfMatchToFinish, awayTeamOfMatchToFinish);
        Team homeTeamOfOtherMatch = new Team("Juventus");
        Team awayTeamOfOtherMatch = new Team("Milan");
        scoreboard.startMatch(homeTeamOfOtherMatch, awayTeamOfOtherMatch);

        scoreboard.finishMatch(homeTeamOfMatchToFinish, awayTeamOfMatchToFinish);

        List<Match> summary = scoreboard.getSummary();
        assertEquals(1, summary.size());
        Match match = summary.get(0);
        assertEquals(homeTeamOfOtherMatch, match.homeTeam());
        assertEquals(awayTeamOfOtherMatch, match.awayTeam());
    }

    @org.junit.jupiter.api.Test
    void getSummary_shouldBeEmpty_whenNoMatchesAreStarted() {
        Scoreboard scoreboard = new Scoreboard();

        assertTrue(scoreboard.getSummary().isEmpty());
    }

    @org.junit.jupiter.api.Test
    void getSummary_shouldReturnAllStartedMatches() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam1 = new Team("Real Madrid");
        Team awayTeam1 = new Team("Barcelona");
        scoreboard.startMatch(homeTeam1, awayTeam1);
        Team homeTeam2 = new Team("Juventus");
        Team awayTeam2 = new Team("Milan");
        scoreboard.startMatch(homeTeam2, awayTeam2);

        List<Match> summary = scoreboard.getSummary();

        assertEquals(2, summary.size());
        assertTrue(summary.stream().anyMatch(
                match -> match.homeTeam().equals(homeTeam1) && match.awayTeam().equals(awayTeam1)
        ));
        assertTrue(summary.stream().anyMatch(
                match -> match.homeTeam().equals(homeTeam2) && match.awayTeam().equals(awayTeam2)
        ));
    }

    @org.junit.jupiter.api.Test
    void getSummary_shouldReturnMatchesOrderedByTotalScoreDescending() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam1 = new Team("Real Madrid");
        Team awayTeam1 = new Team("Barcelona");
        scoreboard.startMatch(homeTeam1, awayTeam1);
        Team homeTeam2 = new Team("Juventus");
        Team awayTeam2 = new Team("Milan");
        scoreboard.startMatch(homeTeam2, awayTeam2);
        Team homeTeam3 = new Team("Bayern");
        Team awayTeam3 = new Team("Dortmund");
        scoreboard.startMatch(homeTeam3, awayTeam3);
        scoreboard.updateScore(homeTeam1, awayTeam1, 1, 1);
        scoreboard.updateScore(homeTeam3, awayTeam3, 2, 2);

        List<Match> summary = scoreboard.getSummary();

        assertEquals(homeTeam3, summary.get(0).homeTeam());
        assertEquals(homeTeam1, summary.get(1).homeTeam());
        assertEquals(homeTeam2, summary.get(2).homeTeam());
    }

    @org.junit.jupiter.api.Test
    void getSummary_shouldReturnMatchesOrderedByWhenTheyWereAddedDescending_whenTheirTotalScoreIsEqual() {
        Scoreboard scoreboard = new Scoreboard();
        Team homeTeam1 = new Team("Real Madrid");
        Team awayTeam1 = new Team("Barcelona");
        scoreboard.startMatch(homeTeam1, awayTeam1);
        Team homeTeam2 = new Team("Juventus");
        Team awayTeam2 = new Team("Milan");
        scoreboard.startMatch(homeTeam2, awayTeam2);
        Team homeTeam3 = new Team("Bayern");
        Team awayTeam3 = new Team("Dortmund");
        scoreboard.startMatch(homeTeam3, awayTeam3);
        scoreboard.updateScore(homeTeam1, awayTeam1, 1, 2);
        scoreboard.updateScore(homeTeam2, awayTeam2, 2, 1);

        List<Match> summary = scoreboard.getSummary();

        assertEquals(homeTeam2, summary.get(0).homeTeam());
        assertEquals(homeTeam1, summary.get(1).homeTeam());
        assertEquals(homeTeam3, summary.get(2).homeTeam());
    }
}