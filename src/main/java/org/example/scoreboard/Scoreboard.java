package org.example.scoreboard;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Scoreboard {
    private List<Match> matches = new ArrayList<>();

    public void startMatch(Team homeTeam, Team awayTeam) {
        Match existingMatch = findMatch(homeTeam, awayTeam);
        if (existingMatch != null) {
            throw new IllegalArgumentException("Match already exists");
        }

        Match match = new Match(homeTeam, awayTeam, 0, 0);
        matches.add(match);
    }

    public void updateScore(Team homeTeam, Team awayTeam, int homeTeamScore, int awayTeamScore) {
        if (homeTeamScore < 0 || awayTeamScore < 0) {
            throw new IllegalArgumentException("Score cannot be negative");
        }

        Match match = findMatch(homeTeam, awayTeam);
        if (match == null) {
            throw new IllegalArgumentException("Match does not exist");
        }

        matches = matches.stream().map(m -> {
            if (m.equals(match)) {
                return new Match(homeTeam, awayTeam, homeTeamScore, awayTeamScore);
            }
            return m;
        }).collect(Collectors.toList());;
    }

    public void finishMatch(Team homeTeam, Team awayTeam) {
        Match match = findMatch(homeTeam, awayTeam);
        if (match == null) {
            throw new IllegalArgumentException("Match does not exist");
        }

        matches.remove(match);
    }

    public List<Match> getSummary() {
        return matches.stream()
                .sorted(Comparator.comparingInt(Match::getTotalScore)
                        .reversed()
                        .thenComparing(Comparator.comparingInt((Match match) -> matches.indexOf(match))
                                .reversed()))
                .collect(Collectors.toList());
    }

    private Match findMatch(Team homeTeam, Team awayTeam) {
        for (Match match : matches) {
            if (match.homeTeam().equals(homeTeam) && match.awayTeam().equals(awayTeam)) {
                return match;
            }
        }
        return null;
    }
}
