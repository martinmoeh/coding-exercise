package org.example.scoreboard;

public record Team(String name) {
    public Team {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
    }
}
