package org.example.scoreboard;

public record Match(Team homeTeam, Team awayTeam, int homeTeamScore, int awayTeamScore) {

    int getTotalScore() {
        return homeTeamScore + awayTeamScore;
    }
}
