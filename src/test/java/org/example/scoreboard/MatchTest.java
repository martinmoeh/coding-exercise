package org.example.scoreboard;

import static org.junit.jupiter.api.Assertions.*;

class MatchTest {
    @org.junit.jupiter.api.Test
    void getTotalScore_shouldReturnTheSumOfBothTeamsScores() {
        Match match = new Match(new Team("Real Madrid"), new Team("Barcelona"), 2, 1);

        assertEquals(3, match.getTotalScore());
    }
}