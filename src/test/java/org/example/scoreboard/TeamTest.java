package org.example.scoreboard;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {
    @org.junit.jupiter.api.Test
    void constructor_shouldThrowAnException_whenTheNameIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> new Team(""));
    }
}