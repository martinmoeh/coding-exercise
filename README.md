# Scoreboard exercise

Assumptions:
- Consumers of the library will not want to work with Match objects apart from when presenting the leaderboard.
  Hence why the parameters for the updateScore() and finishMatch() methods are Team objects instead of Match objects.

Notes:
- I have made an effort to make everything immutable. Notably, the updateScore() method replaces the existing Match
  object instead of updating it in place, avoiding that any returned Match objects are unexpectedly modified. The
  getSummary() method also constructs and returns a new list to avoid finishMatch() removing matches from an already
  returned summary.
- It's been a while since I programmed in Java, so I might not follow all idioms and best practices.